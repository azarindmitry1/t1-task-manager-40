package ru.t1.azarin.tm.exception.user;

public final class PermissionException extends AbstractUserException {

    public PermissionException() {
        super("Error! Permission denied...");
    }

}
