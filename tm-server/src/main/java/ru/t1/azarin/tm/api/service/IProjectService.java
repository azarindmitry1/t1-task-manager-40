package ru.t1.azarin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.enumerated.Status;
import ru.t1.azarin.tm.model.Project;

import java.util.List;

public interface IProjectService {

    @NotNull
    Project add(@Nullable Project project);

    void changeProjectStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    Project create(@Nullable String userId, @Nullable String name);

    @NotNull
    Project create(@Nullable String userId, @Nullable String name, @Nullable String description);

    void clear(@Nullable String userId);

    boolean existById(@Nullable String userId, @Nullable String id);

    @Nullable
    List<Project> findAll(@Nullable String userId);

    @Nullable
    Project findOneById(@Nullable String userId, @Nullable String id);

    void remove(@Nullable Project project);

    void removeById(@Nullable String userId, @Nullable String id);

    void updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

}