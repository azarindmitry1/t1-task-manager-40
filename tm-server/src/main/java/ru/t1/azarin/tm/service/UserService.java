package ru.t1.azarin.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.api.repository.IProjectRepository;
import ru.t1.azarin.tm.api.repository.ITaskRepository;
import ru.t1.azarin.tm.api.repository.IUserRepository;
import ru.t1.azarin.tm.api.service.*;
import ru.t1.azarin.tm.enumerated.Role;
import ru.t1.azarin.tm.exception.entity.EntityNotFoundException;
import ru.t1.azarin.tm.exception.entity.UserNotFoundException;
import ru.t1.azarin.tm.exception.field.EmailEmptyException;
import ru.t1.azarin.tm.exception.field.IdEmptyException;
import ru.t1.azarin.tm.exception.field.LoginEmptyException;
import ru.t1.azarin.tm.exception.field.PasswordEmptyException;
import ru.t1.azarin.tm.exception.user.ExistEmailException;
import ru.t1.azarin.tm.exception.user.ExistLoginException;
import ru.t1.azarin.tm.exception.user.RoleEmptyException;
import ru.t1.azarin.tm.model.User;
import ru.t1.azarin.tm.util.HashUtil;

import java.util.List;

public final class UserService implements IUserService {

    @NotNull
    private final IConnectionService connectionService;

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final ITaskService taskService;

    public UserService(
            @NotNull final IPropertyService propertyService,
            @NotNull final IConnectionService connectionService,
            @NotNull final IProjectService projectService,
            @NotNull final ITaskService taskService
    ) {
        this.connectionService = connectionService;
        this.propertyService = propertyService;
        this.projectService = projectService;
        this.taskService = taskService;
    }

    @Override
    public @NotNull User add(@Nullable final User user) {
        if (user == null) throw new EntityNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            userRepository.add(user);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return user;
    }

    @Override
    public @NotNull User create(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user = new User();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            user.setLogin(login);
            user.setPasswordHash(HashUtil.salt(propertyService, password));
            user.setRole(Role.USUAL);
            userRepository.add(user);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return user;
    }

    @Override
    public @NotNull User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isEmailExist(email)) throw new ExistEmailException();
        @Nullable final User user = new User();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            user.setLogin(login);
            user.setPasswordHash(HashUtil.salt(propertyService, password));
            user.setEmail(email);
            user.setRole(Role.USUAL);
            userRepository.add(user);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return user;
    }

    @Override
    public @NotNull User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        @Nullable final User user = new User();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            user.setLogin(login);
            user.setPasswordHash(HashUtil.salt(propertyService, password));
            user.setRole(role);
            userRepository.add(user);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return user;
    }

    @Override
    public @Nullable List<User> findAll() {
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession();) {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            return userRepository.findAll();
        }
    }

    @Override
    public @Nullable User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            return userRepository.findByLogin(login);
        }
    }

    @Nullable
    @Override
    public User findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            return userRepository.findByEmail(email);
        }
    }

    @Nullable
    @Override
    public User findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            return userRepository.findOneById(id);
        }
    }

    @Override
    public boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            return userRepository.findByLogin(login) != null;
        }
    }

    @Override
    public boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            return userRepository.findByEmail(email) != null;
        }
    }

    @Override
    public void remove(@Nullable final User model) {
        if (model == null) throw new EntityNotFoundException();
        @Nullable final User user;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            @NotNull final String userId = model.getId();
            user = userRepository.findOneById(userId);
            if (user == null) throw new UserNotFoundException();
            userRepository.remove(user);
            if (projectRepository.findAll(userId) != null) projectRepository.clear(userId);
            if (taskRepository.findAll(userId) != null) taskRepository.clear(userId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        remove(user);
    }

    @Override
    public void removeByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable final User user = findByEmail(email);
        if (user == null) throw new UserNotFoundException();
        remove(user);
    }

    @Override
    public void setPassword(@Nullable final String id, @Nullable String password) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            user = userRepository.findOneById(id);
            if (user == null) throw new UserNotFoundException();
            user.setPasswordHash(HashUtil.salt(propertyService, password));
            userRepository.update(user);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            user = userRepository.findByLogin(login);
            if (user == null) throw new UserNotFoundException();
            user.setLocked(true);
            userRepository.update(user);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            user = userRepository.findByLogin(login);
            if (user == null) throw new UserNotFoundException();
            user.setLocked(false);
            userRepository.update(user);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void updateUser(@Nullable final String id,
                           @Nullable final String firstName,
                           @Nullable final String middleName,
                           @Nullable final String lastName
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final User user;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            user = userRepository.findOneById(id);
            if (user == null) throw new UserNotFoundException();
            user.setFirstName(firstName);
            user.setMiddleName(middleName);
            user.setLastName(lastName);
            userRepository.update(user);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

}
